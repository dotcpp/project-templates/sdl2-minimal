#include <app.hpp>

#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>

void App::OnInit()
{
    glClearColor(0.56f, 0.7f, 0.67f, 1.0f);
}

void App::OnResize(int width, int height)
{
    if (height < 1) height = 1;

    glViewport(0, 0, width, height);

    _projection = glm::perspective(glm::radians<float>(90), float(width) / float(height), 0.1f, 1000.0f);
}

void App::OnFrame()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void App::OnExit()
{
}
